import React, { useContext, useState } from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { Text } from 'react-native-elements';
import { Context as SearchContext } from '../context/SearchContext';
import { Context as AuthContext } from '../context/AuthContext';
import Spacer from '../components/Spacer';
import { NavigationEvents } from 'react-navigation';
import { Feather } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import {navigate} from '../navigationRef'


const UserEditScreen = ({navigation}) => {
  const user = navigation.getParam('user');
  const _id = user._id

  const { editUser } = useContext(SearchContext);
  const { state, clearErrorMessage, errorPassword, errorFull } = useContext(AuthContext);

  const [usuario, setUsuario] = useState(user.usuario);
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [email, setEmail] = useState(user.email);

  const comparePassword = () => {
    if (!usuario || !email || !password || !password2){
      errorFull()       
    }
    else if (password == password2){
      editUser({_id, usuario, email,  password})
    }
    else {
      errorPassword()
    }
  };


  return (
    <ScrollView style={styles.container}>
      <NavigationEvents onWillFocus={clearErrorMessage} />
      <TouchableOpacity onPress={()=>{navigate('UserList')}} style={{alignSelf:'flex-end', marginRight:15, marginTop: 45}}>
            <MaterialIcons name="close" size={24} color="black" />
      </TouchableOpacity>
      
      
      <Text style={{alignSelf: 'center', borderBottomWidth:3, borderBottomColor: '#ad2926', marginTop:8}} h4>Editar Usuario</Text>
      <Spacer/>
      <View style={styles.input}>
        <Feather name="user" size={28} color="black" style={{flex:1}}/>
        <TextInput
        placeholder="Usuario"
        value={usuario}
        onChangeText={setUsuario}
        autoCapitalize="none"
        autoCorrect={false}
        style={{paddingLeft:10, flex:9}}
        />
      </View>
      <View style={styles.input}>
        <Feather name="mail" size={28} color="black" style={{flex:1}} />
        <TextInput
        placeholder="Correo Electrónico"
        value={email}
        onChangeText={setEmail}
        autoCapitalize="none"
        autoCorrect={false}
        style={{flex:9, paddingLeft: 10}}
        />
      </View>
      <View style={styles.input}>
        <Feather name="unlock" size={28} color="black" style={{flex:1}}/>
        <TextInput
        secureTextEntry
        placeholder="Contraseña"
        value={password}
        onChangeText={setPassword}
        autoCapitalize="none"
        autoCorrect={false}
        style={{flex:9, paddingLeft: 10}}
        />
      </View>
      <View style={styles.input}>
        <Feather name="unlock" size={28} color="black" style={{flex:1}}/>
        <TextInput
        secureTextEntry
        placeholder="Contraseña"
        value={password2}
        onChangeText={setPassword2}
        autoCapitalize="none"
        autoCorrect={false}
        style={{flex:9, paddingLeft: 10}}
        />
      </View>

      {state.errorMessage ? (
        <Text style={styles.errMessage}>{state.errorMessage}</Text>
      ) : null}

      <TouchableOpacity
          onPress={comparePassword}
          style= {styles.sendIcon}
      >
          <MaterialIcons name="arrow-forward" size={40} color="white" />
      </TouchableOpacity>
    </ScrollView>
  );
};

UserEditScreen.navigationOptions = {
  header: () => false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  errMessage: {
    fontSize: 16,
    color: '#ad2926',
    alignSelf: 'center'
  },
  input: {
    borderColor: 'grey',
    borderWidth: 2,
    borderRadius: 7,
    height:40,
    marginHorizontal: 15,
    marginVertical: 10,
    paddingLeft:10,
    paddingVertical: 8,
    flexDirection:'row',
    alignItems: 'center'
  },
  sendIcon: {
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:50,
    height: 50, 
    justifyContent: 'center',
    alignSelf:'flex-end', 
    alignItems: 'center',
    marginRight:30
  }
});

export default UserEditScreen;