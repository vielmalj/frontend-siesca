import React, { useContext } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Context as SearchContext } from '../context/SearchContext';
import ProductForm from '../components/ProductForm';
import { MaterialIcons } from '@expo/vector-icons';


const ProductEditScreen = ({navigation})=>{

    const product= navigation.getParam('product');

    const {createProduct} = useContext(SearchContext);
    const _id = product._id

    return (
        <View style={styles.container}>
            <ProductForm
                initialValues={{nameProduct: product.nameProduct, stock: product.stock, description: product.description}}
                headerText= "Editar Producto"
                onSubmit={({nameProduct, stock, description, photo})=> {
                    createProduct ({_id, nameProduct, stock, description, photo})}}
            />
        </View>
    )
};

ProductEditScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
});

export default ProductEditScreen;