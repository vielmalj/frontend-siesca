import React, {useContext} from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { Text} from 'react-native-elements';
import {Context as SearchContext} from '../context/SearchContext';
import {EvilIcons} from '@expo/vector-icons';



const ClientDetailScreen = ({ navigation }) => {

  const { state, deleteClient} = useContext(SearchContext);
  const _id = navigation.getParam('_id');

  const client = state.find(prod => prod._id === _id);


return (
  <View style={{flex:1, backgroundColor:'8e5555', marginTop:40}}>
      <Text style={styles.text} h4>Nombre:  {client.nameClient}</Text>
      <Text style={styles.text} h4>Apellido:  {client.lastName}</Text>
      <Text style={styles.text} h4>Dirección: {client.adress}</Text>
      <Text style={styles.text} h4>Identificacion: {client.idClient}</Text>

      <TouchableOpacity 
          onPress= {()=> {deleteClient({_id})}} 
          style={{position: 'absolute', bottom:20, right: 18}}> 
        <View style={styles.editIcon}>
          <EvilIcons name="trash" size={50} color="white" />       
        </View>    
      </TouchableOpacity> 

      <TouchableOpacity 
              onPress={() => 
                navigation.navigate('ClientEdit', {client: client})} 
              style={{position: 'absolute', bottom:100, right: 18}}>
                <View style={styles.editIcon}>
                   <EvilIcons name="pencil" size= {50} color="white"/>
                </View>
      </TouchableOpacity >
  </View>
  
)};

ClientDetailScreen.navigationOptions = () => {
  return {       
    title: 'Cliente',
    headerTitleStyle: { alignSelf: 'center', color: 'white'},
    headerStyle: {backgroundColor: '#74787c'}
  };
};


const styles = StyleSheet.create({
  editIcon: {
    marginRight: 10,
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:60,
    height: 60,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  text: {
    marginBottom: 5
  },
  iconStyle: {
    marginLeft: 10,
    backgroundColor: '#74787c',
    borderRadius: 40,
    width:35,
    height: 35,
    justifyContent: 'center', 
    alignItems: 'center'
  }
});

export default ClientDetailScreen;