import React, {useContext} from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import {Context as SearchContext} from '../context/SearchContext';
import { Text} from 'react-native-elements';
import {EvilIcons} from '@expo/vector-icons';


const UserDetailScreen = ({ navigation }) => {
  const { state, deleteUser } = useContext(SearchContext);
  const _id = navigation.getParam('_id');

  const user = state.find(prod => prod._id === _id);

  return ( 
    <View style={{flex:1, backgroundColor:'8e5555', marginTop:40}}>
      <Text style={styles.text} h4>Usuario:  {user.usuario}</Text>
      <Text style={styles.text} h4>Email:  {user.email}</Text>
    
      <TouchableOpacity 
        onPress= {()=> {deleteUser({_id})}} 
         style={{position: 'absolute', bottom:20, right: 18}}> 
        <View style={styles.editIcon}>
             <EvilIcons name="trash" size={50} color="white" />       
         </View>    
      </TouchableOpacity> 
      <TouchableOpacity 
        onPress={()=>{navigation.navigate('UserEdit', { user: user})}} 
         style={{position: 'absolute', bottom:100, right: 18}}> 
        <View style={styles.editIcon}>
             <EvilIcons name="pencil" size={50} color="white" />       
         </View>    
      </TouchableOpacity>
    
    </View>
    )}

UserDetailScreen.navigationOptions = () => {
    return {       
        title: 'Usuario',
        headerTitleStyle: { alignSelf: 'center', color: 'white'},
        headerStyle: {backgroundColor: '#74787c'}
      };
  };

const styles = StyleSheet.create({
  editIcon: {
    marginRight: 10,
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:60,
    height: 60,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  text: {
    marginBottom: 5
  }
});

export default UserDetailScreen;