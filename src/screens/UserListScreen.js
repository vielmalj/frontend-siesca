import React, {useContext, useEffect} from 'react';
import { View, StyleSheet, Text, TouchableOpacity, FlatList } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { NavigationEvents } from 'react-navigation';
import {Context as SearchContext} from '../context/SearchContext';
import { AntDesign } from '@expo/vector-icons';
import SearchBar from '../components/SearchBar'


const UserListScreen = ({ navigation }) => {

  const { state, searchUser, searchUserSumit } = useContext(SearchContext);

  useEffect(()=> {
    searchUser();
    const listener = navigation.addListener ('didFocus', () => {
      searchUser();
    });
    return () => {
      listener.remove();
    };
  }, [])
  

  return (
    <>
      <SearchBar onTermSubmit={searchUserSumit}/>
      <FlatList 
        data={state}
        keyExtractor={item => item._id}
        renderItem={({ item }) => {
          return(
            <TouchableOpacity onPress={() =>
              navigation.navigate('UserDetail', { _id: item._id })
            }>
              <View style={styles.container}>
                  <View  style= {styles.icon2Style}>
                    <AntDesign name="user" size={24} color="white" />
                  </View>
                  <View style={{marginLeft:15}}>
                    <Text style={styles.name}>{item.usuario}</Text>
                    <Text>{item.email}</Text>
                  </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />

    </>
  );
};

UserListScreen.navigationOptions = ({ navigation }) => {
  return {       
    headerLeft: () => (
      <TouchableOpacity 
             onPress={() => 
              navigation.openDrawer()}
              style= {styles.iconStyle}
      > 
          <FontAwesome name="bars" size={20} color= "white" /> 
      </TouchableOpacity> ),
      title: 'Usuarios',
      headerTitleStyle: { alignSelf: 'center', color: 'white'},
      headerStyle: {backgroundColor: '#74787c'}
  };
};

const styles = StyleSheet.create({
  iconStyle: {
    marginLeft: 10,
    backgroundColor: '#74787c',
    borderRadius: 40,
    width:35,
    height: 35,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  icon2Style: {
    marginLeft: 10,
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:45,
    height: 45,
    justifyContent: 'center', 
    alignItems: 'center'
    
  },
  container: {
    flexDirection: 'row',
    backgroundColor: '#D9DBDC',
    margin: 10,
    padding: 7,
    alignItems: 'center',
    borderLeftWidth:5,
    borderLeftColor: '#ad2926'
  }, 
  name: {
    fontSize: 24,
    fontWeight: 'bold'
  }
});

export default UserListScreen;