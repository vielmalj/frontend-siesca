import React, { useContext } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Context as SearchContext } from '../context/SearchContext';
import ProductForm from '../components/ProductForm';


const ProductCreateScreen = (navigation)=>{
    const {createProduct} = useContext(SearchContext);
    return (
        <View style={styles.container}>
            <ProductForm
                headerText= "Crear Producto"
                onSubmit={(nameProduct, stock, description, photo)=> {createProduct (nameProduct, stock, description, photo)}}
            />
        </View>
    )
};

ProductCreateScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    }
});

export default ProductCreateScreen;