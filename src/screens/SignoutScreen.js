import React, { useContext } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Text } from 'react-native-elements';
import { Context as AuthContext } from '../context/AuthContext';

const SignoutScreen = () => {
  const { signout } = useContext(AuthContext);

  return (
    <View style={{flex:1, backgroundColor: '#74787c', justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{ fontSize: 35, color: 'white'}}>¿Desea cerrar sesión?</Text>
      <TouchableOpacity
        onPress={signout}
        style= {styles.sendIcon}
      >
        <Text style={{color: '#ad2926'}}>Cerrar Sesión</Text>
      </TouchableOpacity>
    </View>
    
  );
};

const styles = StyleSheet.create({
  sendIcon: {
    backgroundColor: 'white',
    borderRadius: 40,
    width:280,
    height: 50, 
    justifyContent: 'center', 
    alignItems: 'center',
    marginTop: 40
  }
});

export default SignoutScreen;
