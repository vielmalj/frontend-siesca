import React, {useContext, useEffect} from 'react';
import { StyleSheet, FlatList, TouchableOpacity, Text, View, Image } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { NavigationEvents } from 'react-navigation';
import {Context as SearchContext} from '../context/SearchContext';
import { MaterialIcons } from '@expo/vector-icons';
import SearchBar from '../components/SearchBar'


const ProductListScreen = ({ navigation }) => {

  const { state, searchProduct, searchProductSumit } = useContext(SearchContext);

  useEffect(()=> {
    searchProduct();
    const listener = navigation.addListener ('didFocus', () => {
      searchProduct();
    });
    return () => {
      listener.remove();
    };
  }, [])

  return (
    <>
      <SearchBar onTermSubmit={searchProductSumit}/>
      <FlatList 
        data={state}
        keyExtractor={item => item._id}
        renderItem={({ item }) => {
          return(
            <TouchableOpacity onPress={() =>
              navigation.navigate('ProductDetail', { _id: item._id })
            }>
              <View style={styles.container}> 
                <View style={{flex:4}}>
                  <Image source={require('../../assets/iconos-01.png')} style={{ width: 100, height: 100}}/>
                </View> 
                <View style={{flex:6}}>            
                  <Text style={styles.name}>{item.nameProduct}</Text>
                  <Text style={styles.idStyle}>{item.description}</Text>
                </View>
                <View style={styles.stockStyle}>
                  <Text style={{fontSize:20}}>{item.stock}</Text>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />

      <TouchableOpacity 
          onPress={() => navigation.navigate('ProductCreate')} 
      >
          <View style={styles.iconStyle2}>
              <MaterialIcons name="add" size={45} color="white" />
          </View>
      </TouchableOpacity >

    </>
  );
};

ProductListScreen.navigationOptions = ({ navigation }) => {
  return {       
    headerLeft: () => (
      <TouchableOpacity 
             onPress={() => 
              navigation.openDrawer()}
              style= {styles.iconStyle}
      > 
          <FontAwesome name="bars" size={20} color= "white"/> 
      </TouchableOpacity> ),
      title: 'Productos',
      headerTitleStyle: { alignSelf: 'center', color: 'white'},
      headerStyle: {backgroundColor: '#74787c'}
    };
};

const styles = StyleSheet.create({
  iconStyle: {
    marginLeft: 10,
    backgroundColor: '#74787c',
    borderRadius: 40,
    width:35,
    height: 35,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  iconStyle2: {
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:60,
    height: 60,
    justifyContent: 'center', 
    alignItems: 'center',
    alignSelf: 'flex-end',
    bottom:20,
    right: 20
  },
  image: {
    width: 250,
    height: 120,
    borderRadius: 4,
    marginBottom: 5
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  container: {
    flexDirection: 'row',
    backgroundColor: '#D9DBDC',
    margin: 10,
    padding: 7,
    alignItems: 'center',
    borderLeftWidth:5,
    borderLeftColor: '#ad2926'
  },
  stockStyle: {
    marginLeft: 10,
    backgroundColor: '#8e5555',
    borderRadius: 40,
    width:50,
    height: 50,
    justifyContent: 'center', 
    alignItems: 'center',
    flex:2
  },
});

export default ProductListScreen;
