import React, {useContext} from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { Text} from 'react-native-elements';
import {Context as SearchContext} from '../context/SearchContext';
import {EvilIcons} from '@expo/vector-icons';


const ProductDetailScreen = ({ navigation }) => {
  const { state, deleteProduct } = useContext(SearchContext);
  const _id = navigation.getParam('_id');

  const product = state.find(prod => prod._id === _id);

return ( 
<View style={{flex:1, backgroundColor:'8e5555', marginTop:40}}>
  <Text style={styles.text} h4>Nombre Producto:  {product.nameProduct}</Text>
  <Text style={styles.text} h4>Cantidad:  {product.stock}</Text>
  <Text style={styles.text} h4>Descripción: {product.description}</Text>

  <TouchableOpacity 
    onPress= {()=> {deleteProduct({_id})}} 
     style={{position: 'absolute', bottom:20, right: 18}}> 
    <View style={styles.editIcon}>
         <EvilIcons name="trash" size={50} color="white" />       
     </View>    
  </TouchableOpacity> 

  <TouchableOpacity 
      onPress={() => 
          navigation.navigate('ProductEdit', {product : product})} 
      style={{position: 'absolute', bottom:100, right: 18}}>
      <View style={styles.editIcon}>
        <EvilIcons name="pencil" size= {50} color="white"/>
      </View>
  </TouchableOpacity >

</View>
)}

ProductDetailScreen.navigationOptions = () => {
  return {       
    title: 'Producto',
    headerTitleStyle: { alignSelf: 'center', color: 'white'},
    headerStyle: {backgroundColor: '#74787c'}
  };
};


const styles = StyleSheet.create({
  editIcon: {
    marginRight: 10,
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:60,
    height: 60,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  text: {
    marginBottom: 5
  }
});

export default ProductDetailScreen;
