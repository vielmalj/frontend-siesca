import React, { useContext } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Context as SearchContext } from '../context/SearchContext';
import ClientForm from '../components/ClientForm';


const ClientEditScreen = ({navigation}) => {

    const client = navigation.getParam('client');

    const {editClient} = useContext(SearchContext);
    const _id = client._id

    return (
        <View style={styles.container}>
            <ClientForm
                initialValues= {{nameClient: client.nameClient, lastName: client.lastName, adress: client.adress, idClient: client.idClient }}
                headerText= "Editar Cliente"
                onSubmit={({nameClient, lastName, adress, idClient}) => {
                    editClient({_id, nameClient, lastName, adress, idClient})
                }}
            />
        </View>
    )
};

ClientEditScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
});

export default ClientEditScreen;

