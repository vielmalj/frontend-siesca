import React, { useContext, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, Image, ScrollView } from 'react-native';
import { Text} from 'react-native-elements';
import { NavigationEvents } from 'react-navigation';
import { Context as AuthContext } from '../context/AuthContext';
import NavLink from '../components/NavLink';
import { Feather } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';


const SigninScreen = () => {
  const [usuario, setUsuario] = useState('');
  const [password, setPassword] = useState('');
  const { state, signin, clearErrorMessage, errorFull } = useContext(AuthContext);

  const allData = () => {
    if (!usuario || !password){
      errorFull()       
    }
    else {
       signin({ usuario, password })
    }
  };

  return (
    <>
    <ScrollView style={styles.container}>
      <NavigationEvents onWillFocus={clearErrorMessage} />
      <LinearGradient
        // Background Linear Gradient
        colors={['rgba(0,0,0,0.8)', 'transparent']}
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          height: 300,
        }}
      />
      <Image source={require('../../assets/iconos-02.png')} style={{height: 140, width:140, alignSelf: 'center',marginTop: 80}}></Image>
      <Text style={{alignSelf: 'center', borderBottomWidth:3, borderBottomColor: 'white', color: 'white'}} h3>Siesca</Text>
      <Text style={{alignSelf: 'center', color: 'white', marginBottom: 25}} h4>Inicio de sesión</Text>
      <View style={styles.input}>
      <Feather name="user" size={28} color="white" style={{flex:1}}/>
      <TextInput
        placeholder="Usuario"
        value={usuario}
        onChangeText={setUsuario}
        autoCapitalize="none"
        autoCorrect={false}
        style={{paddingLeft:10, flex:9}}
      />
      </View>
      <View style={styles.input}>
      <Feather name="unlock" size={28} color="white" style={{flex:1}}/>
      <TextInput
        secureTextEntry
        placeholder="Contraseña"
        value={password}
        onChangeText={setPassword}
        autoCapitalize="none"
        autoCorrect={false}
        style={{flex:9, paddingLeft: 10}}
      />
      </View>
      {state.errorMessage ? (
        <Text style={styles.errMessage}>{state.errorMessage}</Text>
      ) : null}
        <TouchableOpacity
          onPress={allData}
          style= {styles.sendIcon}
        >
            <Text h4 style={{color: '#ad2926'}}>Iniciar</Text>
        </TouchableOpacity>
    </ScrollView>
    <View style= {{backgroundColor: '#74787c', borderBottomWidth: 8, borderBottomColor: '#ad2926', alignItems: 'center'}}>
    <NavLink
        text1="¿No tienes cuenta?" 
        text2= "Registrate"
        routeName="Signup"
    />
    </View>
    </>
  );
};

SigninScreen.navigationOptions = {
  header: () => false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#74787c'
    
  },
  errMessage: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center'
  },
  input: {
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 7,
    height:50,
    marginHorizontal: 30,
    paddingLeft:10,
    flexDirection:'row',
    alignItems: 'center',
    marginBottom: 15
  },
  sendIcon: {
    backgroundColor: 'white',
    borderRadius: 40,
    width:280,
    height: 50, 
    justifyContent: 'center',
    alignSelf:'center', 
    alignItems: 'center',
    marginTop:20
  }
});

export default SigninScreen;
