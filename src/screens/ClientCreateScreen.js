import React, { useContext } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Context as SearchContext } from '../context/SearchContext';
import ClientForm from '../components/ClientForm';


const ClientCreateScreen = (navigation) => {
    
    const {createClient} = useContext(SearchContext);
    return (
        <View style={styles.container}>
            <ClientForm
                headerText= "Crear Cliente"
                onSubmit={(nameClient, lastName, adress, idClient)=> {createClient (nameClient, lastName, adress, idClient)}}
            />
        </View>
    )
};

ClientCreateScreen.navigationOptions = () => {
    return {
        header: () => false,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
});

export default ClientCreateScreen;