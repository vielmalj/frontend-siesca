import React, {useContext, useEffect} from 'react';
import {StyleSheet,  TouchableOpacity, FlatList, View } from 'react-native';
import { Text} from 'react-native-elements';
import {Context as SearchContext} from '../context/SearchContext';
import { Entypo } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import {EvilIcons} from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import SearchBar from '../components/SearchBar'


const ClientListScreen = ({ navigation }) => {

  const { state, searchClient, searchClientSumit} = useContext(SearchContext);

  useEffect(()=> {
    searchClient();
    const listener = navigation.addListener ('didFocus', () => {
      searchClient();
    });
    return () => {
      listener.remove();
    };
  }, [])

  return (
    <>
      <SearchBar onTermSubmit={searchClientSumit}/>
      <FlatList 
        data={state}
        keyExtractor={item => item._id}
        renderItem={({ item }) => {
          return(
                <TouchableOpacity onPress={() =>
                  navigation.navigate('ClientDetail', { _id: item._id })}
                >
                  <View style={styles.container}>
                    <View  style= {styles.icon2Style}>
                      <Entypo name="network" size={24} color="white" />   
                    </View>  
                    <View style={{marginLeft:15}}>            
                      <Text style={styles.name}>{item.nameClient}</Text>
                      <Text style={styles.idStyle}>{item.idClient}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
          );
        }}
      />

      <TouchableOpacity 
          onPress={() => navigation.navigate('ClientCreate')} 
          style={styles.modal}
      >
          <View style={styles.iconStyle2}>
              <MaterialIcons name="add" size={45} color="white" />
          </View>
      </TouchableOpacity >
    </>
  );
};

ClientListScreen.navigationOptions = ({ navigation }) => {
  return {       
    headerLeft: () => (
      <TouchableOpacity 
             onPress={() => 
              navigation.openDrawer()}
              style= {styles.iconStyle}
      > 
          <FontAwesome name="bars" size={20} color= "white" /> 
      </TouchableOpacity> ),
    title: 'Clientes',
    headerTitleStyle: { alignSelf: 'center', color: 'white'},
    headerStyle: {backgroundColor: '#74787c'}
  };
};

const styles = StyleSheet.create({
  iconStyle: {
    marginLeft: 10,
    backgroundColor: '#74787c',
    borderRadius: 40,
    width:35,
    height: 35,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  icon2Style: {
    marginLeft: 10,
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:45,
    height: 45,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  iconStyle2: {
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:60,
    height: 60,
    justifyContent: 'center', 
    alignItems: 'center',
    alignSelf: 'flex-end',
    bottom:20,
    right: 20
  },
  sendIcon: {
    backgroundColor: '#ad2926',
    borderRadius: 40,
    width:50,
    height: 50, 
    justifyContent: 'center',
    alignSelf:'flex-end', 
    alignItems: 'center',
    marginRight:30
  },
  container: {
    flexDirection: 'row',
    backgroundColor: '#D9DBDC',
    margin: 10,
    padding: 7,
    alignItems: 'center',
    borderLeftWidth:5,
    borderLeftColor: '#ad2926'
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  input: {
    borderColor: 'grey',
    borderWidth: 2,
    borderRadius: 7,
    height:40,
    margin: 15,
    paddingLeft:10
  }
});

export default ClientListScreen;
