import React, { useState, useContext } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View, ScrollView } from 'react-native';
import { Text } from 'react-native-elements';
import { NavigationEvents } from 'react-navigation';
import Spacer from './Spacer';
import { MaterialIcons } from '@expo/vector-icons';
import {navigate} from '../navigationRef'
import { Context as AuthContext } from '../context/AuthContext';

const ClientForm = ({ headerText,  onSubmit, initialValues}) => {
    const [nameClient, setNameClient] =useState(initialValues.nameClient);
    const [lastName, setLastName] =useState(initialValues.lastName);
    const [adress, setAdress] =useState(initialValues.adress);
    const [idClient, setIdClient] =useState(initialValues.idClient);

    const { state, clearErrorMessage, errorFull } = useContext(AuthContext);

    const error1 = ()=>{
      if (!nameClient || !lastName || !adress || !idClient){
        errorFull()        
      }
      else {
        {onSubmit({ nameClient, lastName, adress, idClient})}
      }
    }

  return (
    <ScrollView>
      <NavigationEvents onWillFocus={clearErrorMessage} />
      <View style={{flex:1}}> 
         <TouchableOpacity onPress={()=>{navigate('ClientList')}} style={{alignSelf:'flex-end', marginRight:15, marginTop: 45}}>
            <MaterialIcons name="close" size={24} color="black" />
         </TouchableOpacity>
           <Text style={{alignSelf: 'center', borderBottomWidth:3, borderBottomColor: '#ad2926', marginTop:8}} h4>{headerText}</Text>
           <Spacer/>

         <TextInput
          placeholder= 'Nombre'
          value={nameClient}
          onChangeText={setNameClient}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.input}
        />
        <TextInput
          placeholder="Apellido"
          value={lastName}
          onChangeText={setLastName}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.input}
        />
        <TextInput
          placeholder="Direccion"
          value={adress}
          onChangeText={setAdress}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.input}
        />
        <TextInput
          placeholder="Documento de Identidad"
          value={`${idClient}`}
          onChangeText={setIdClient}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.input}
        />

        <Spacer/>

        {state.errorMessage ? (
        <Text style={styles.errMessage}>{state.errorMessage}</Text>
        ) : null}        
        <TouchableOpacity
          onPress={error1}
          style= {styles.sendIcon}
        >
            <MaterialIcons name="arrow-forward" size={40} color="white" />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

ClientForm.defaultProps = {
    initialValues: {
        nameClient: '',
        lastName: '',
        adress: '',
        idClient:'',
    }
};

const styles = StyleSheet.create({
    iconStyle: {
      marginRight: 10,
      backgroundColor: '#74787c',
      borderRadius: 40,
      width:35,
      height: 35,
      justifyContent: 'center', 
      alignItems: 'center'
    },
    errMessage: {
      fontSize: 16,
      color: '#ad2926',
      alignSelf: 'center'
    },
    sendIcon: {
      backgroundColor: '#ad2926',
      borderRadius: 40,
      width:50,
      height: 50, 
      justifyContent: 'center',
      alignSelf:'flex-end', 
      alignItems: 'center',
      marginRight:30
    },
    container: {
      flexDirection: 'row',
      backgroundColor: '#7d7d7d',
      margin: 10,
      padding: 7
    },
    name: {
      fontSize: 24,
      fontWeight: 'bold'
    },
    modal: {
      position: 'absolute',                                          
      bottom:90,
      right: 18
    },
    input: {
      borderColor: 'grey',
      borderWidth: 2,
      borderRadius: 7,
      height:40,
      marginHorizontal: 15,
      marginVertical: 10,
      paddingLeft:10,
      paddingVertical: 5
    }
  });

export default ClientForm;
