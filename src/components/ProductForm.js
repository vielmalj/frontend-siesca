import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View, Image, Platform, Button, ScrollView} from 'react-native';
import { Text } from 'react-native-elements';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import Spacer from './Spacer';
import { MaterialIcons } from '@expo/vector-icons';
import {navigate} from '../navigationRef';
import { Context as AuthContext } from '../context/AuthContext';
import { NavigationEvents } from 'react-navigation';


const ProductForm = ({ headerText,  onSubmit, initialValues, screen}) => {

    const [nameProduct, setNameProduct] =useState(initialValues.nameProduct);
    const [stock, setStock] =useState(initialValues.stock);
    const [description, setDescription] =useState(initialValues.description);
    const [photo, setPhoto] =useState(initialValues.photo);
    
    const { state, clearErrorMessage, errorFull2 } = useContext(AuthContext);

    const error2 = ()=>{
      if (!nameProduct || !stock || !description ){
        errorFull2()        
      }
      else {
        {onSubmit({ nameProduct, stock, description, photo})}
      }
    }

    useEffect(() => {
      (async () => {
        if (Platform.OS !== 'web') {
          const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
          if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
          }
        }
      })();
    }, []);

    const pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        setPhoto(result.uri);
      }
    };

  return (
    <ScrollView>
      <NavigationEvents onWillFocus={clearErrorMessage} />
      <View style={{flex:1}}>  
      <TouchableOpacity onPress={()=>{navigate('ProductList')}} style={{alignSelf:'flex-end', marginRight:15, marginTop: 45}}>
                <MaterialIcons name="close" size={24} color="black" />
       </TouchableOpacity>  
        <Text style={{alignSelf: 'center', borderBottomWidth:3, borderBottomColor: '#ad2926'}} h4>{headerText}</Text>
        <Spacer/>

        <TextInput
          placeholder= 'Nombre de Producto'
          value={nameProduct}
          onChangeText={setNameProduct}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.input}
        />
        <TextInput
          placeholder="Cantidad"
          value={`${stock}`}
          onChangeText={setStock}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.input}
        />
        <TextInput
          placeholder="Descripción"
          value={description}
          onChangeText={setDescription}
          autoCapitalize="none"
          autoCorrect={false}
          style={styles.inputDescription}
        />
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TouchableOpacity onPress={pickImage}>
            <View style={{backgroundColor: '#D9DBDC', marginVertical: 15, width: 120, marginLeft:15}}>
              <Text style={{fontSize:20, paddingVertical:5, alignSelf: 'center'}}>Seleccionar</Text>
            </View>
          </TouchableOpacity>
          {photo && <Image source={{ uri: photo }} style={{ width: 200, height: 200, alignSelf: 'center' }} />}
        </View>

        <Spacer/>

        {state.errorMessage ? (
        <Text style={styles.errMessage}>{state.errorMessage}</Text>
        ) : null}
        
        <TouchableOpacity
          onPress={error2}
          style= {styles.sendIcon}
        >
            <MaterialIcons name="arrow-forward" size={40} color="white" />
        </TouchableOpacity>
        </View> 
    </ScrollView>
  );
};

ProductForm.defaultProps = {
    initialValues: {
        nameProduct: '',
        stock : '',
        description: '',
        photo: null,
    }
};

const styles = StyleSheet.create({
    iconStyle: {
      marginRight: 10,
      backgroundColor: '#74787c',
      borderRadius: 40,
      width:35,
      height: 35,
      justifyContent: 'center', 
      alignItems: 'center'
    },
    errMessage: {
      fontSize: 16,
      color: '#ad2926',
      alignSelf: 'center'
    },
    sendIcon: {
      backgroundColor: '#ad2926',
      borderRadius: 40,
      width:50,
      height: 50, 
      justifyContent: 'center',
      alignSelf:'flex-end', 
      alignItems: 'center',
      marginRight:30
    },
    container: {
      flexDirection: 'row',
      backgroundColor: '#7d7d7d',
      margin: 10,
      padding: 7
    },
    name: {
      fontSize: 24,
      fontWeight: 'bold'
    },
    modal: {
      position: 'absolute',                                          
      bottom:90,
      right: 18
    },
    input: {
      borderColor: 'grey',
      borderWidth: 2,
      borderRadius: 7,
      marginHorizontal: 15,
      marginVertical: 10,
      paddingLeft:10,
      paddingVertical:5
    },
    inputDescription: {
      borderColor: 'grey',
      borderWidth: 2,
      borderRadius: 7,
      marginHorizontal: 15,
      marginVertical: 10,
      paddingLeft:10,
      paddingVertical:5,
      height: 100
    }
  });

export default ProductForm;
