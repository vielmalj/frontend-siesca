import React from 'react';
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native';
import Spacer from './Spacer';
import { withNavigation } from 'react-navigation';

const NavLink = ({ navigation, text1, text2, routeName }) => {
  return (
    <TouchableOpacity onPress={() => navigation.navigate(routeName)}>
      <Spacer>
        <View style={{flexDirection: 'row'}}>
        <Text style={{color: 'white', padding:4}}>{text1}</Text>
        <Text style={{color: 'white', padding:4, fontWeight:'bold'}}>{text2}</Text>
        </View>
      </Spacer>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({});

export default withNavigation(NavLink);
