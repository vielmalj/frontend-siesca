import React, {useState} from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';

const SearchBar = ({onTermSubmit}) => {

    const [term, setTerm] = useState('');

  return (
    <View style={styles.backgroundStyle}>
      <Feather name="search" style={styles.iconStyle} />
      <TextInput
        autoCapitalize="none"
        autoCorrect={false}
        style={styles.inputStyle}
        placeholder="buscar"
        value={term}
        onChangeText={setTerm}
        onEndEditing={()=> {onTermSubmit({term})}}
      />

    </View>
  );
};

const styles = StyleSheet.create({
  backgroundStyle: {
    marginTop: 10,
    backgroundColor: '#D9DBDC',
    paddingVertical: 5,
    borderRadius: 30,
    marginHorizontal: 15,
    flexDirection: 'row',
    marginBottom: 10
  },
  inputStyle: {
    flex: 1,
    fontSize: 18
  },
  iconStyle: {
    fontSize: 28,
    alignSelf: 'center',
    marginHorizontal: 15,
    color: 'white'
  }
});

export default SearchBar;