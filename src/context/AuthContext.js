import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import trackerApi from '../api/tracker';
import { navigate } from '../navigationRef';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'add_error':
      return { ...state, errorMessage: action.payload };
    case 'signin':
      return { errorMessage: '', token: action.payload };
    case 'clear_error_message':
      return { ...state, errorMessage: '' };
    case 'signout':
      return { token: null, errorMessage: '' };
    default:
      return state;
  }
};

const tryLocalSignin = dispatch => async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    dispatch({ type: 'signin', payload: token });
    navigate('ProductList');
  } else {
    navigate('Signup');
  }
};

const errorPassword= dispatch => () => {
  dispatch({type: 'add_error',  payload: 'Las contraseñas deben ser iguales' })
};

const errorFull = dispatch => ()=> {
  dispatch ({type: 'add_error',  payload: 'Debe llenar todos los datos' })
};

const errorFull2 = dispatch => ()=> {
  dispatch ({type: 'add_error',  payload: 'El nombre del producto, cantidad y descripción son datos obligatorios' })
};

const clearErrorMessage = dispatch => () => {
  dispatch({ type: 'clear_error_message' });
};

const signup = dispatch => async ({ usuario, email, password }) => {
  try {
    const response = await trackerApi.post('/signup', { usuario, email, password });
    await AsyncStorage.setItem('token', response.data.token);
    dispatch({ type: 'signin', payload: response.data.token });

    navigate('ProductList');
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Algo salio mal con el registro'
    });
  }
};

const signin = dispatch => async ({ usuario, password }) => {
  try {
    const response = await trackerApi.post('/signin', { usuario, password });
    await AsyncStorage.setItem('token', response.data.token);
    dispatch({ type: 'signin', payload: response.data.token });
    navigate('ProductList');
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Usuario o contraseña invalidos'
    });
  }
};

const signout = dispatch => async () => {
  await AsyncStorage.removeItem('token');
  dispatch({ type: 'signout' });
  navigate('loginFlow');
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signin, signout, signup, clearErrorMessage, tryLocalSignin, errorPassword, errorFull, errorFull2 },
  { token: null, errorMessage: '' }
);
