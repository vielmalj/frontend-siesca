import createDataContext from './createDataContext';
import trackerApi from '../api/tracker';
import { navigate } from '../navigationRef';

const searchReducer = (state, action) => {
    switch (action.type) {
      case 'search_product':
        return action.payload;
      case 'search_client':
        return action.payload;
      case 'search_user':
        return action.payload;
      case 'search_clientsumit':
        return action.payload;
      case 'search_productsumit':
        return action.payload;
      case 'search_usersumit':
        return action.payload;
      default:
        return state;
  }
};

const searchProduct = dispatch => async () => {
  const response = await trackerApi.get('/products');
  dispatch({type: 'search_product', payload: response.data });
};
const searchClient = dispatch => async () => {
  const response = await trackerApi.get('/clients');
  dispatch({type: 'search_client', payload: response.data });
};
const searchUser = dispatch => async () => {
  const response = await trackerApi.get('/users');
  dispatch({type: 'search_user', payload: response.data });
};


const searchClientSumit = dispatch => async ({term}) => {
  const response = await trackerApi.get(`/clients/${term}`);
  dispatch({type: 'search_clientsumit', payload: response.data });
};
const searchProductSumit = dispatch => async ({term}) => {
  const response = await trackerApi.get(`/products/${term}`);
  dispatch({type: 'search_productsumit', payload: response.data });
};
const searchUserSumit = dispatch => async ({term}) => {
  const response = await trackerApi.get(`/users/${term}`);
  dispatch({type: 'search_usersumit', payload: response.data });
};


const editUser = dispatch => async ({usuario, email, password, _id}) => {
  await trackerApi.post(`/users/edit/${_id}`, {usuario, email, password});
  navigate('UserList');
};
const editProduct = dispatch => async ({nameProduct, stock, description, photo, _id}) => {
  await trackerApi.post(`/products/edit/${_id}`, {nameProduct, stock, description, photo});
  navigate('ProductList');
};
const editClient = dispatch => async ({nameClient, lastName, adress, idClient, _id}) => {
  await trackerApi.post(`/clients/edit/${_id}`, {nameClient, lastName, adress, idClient});
  navigate('ClientList');
};


const createClient = dispatch  => async ({nameClient, lastName, adress, idClient}) => {
  await trackerApi.post('/add/clients', {nameClient, lastName, adress, idClient});
  navigate('ClientList')
};
const createProduct = dispatch  => async ({nameProduct, stock, description, photo}) => {
  await trackerApi.post('/add/products', {nameProduct, stock, description, photo});
  navigate('ProductList')
};


const deleteClient = dispatch => async ({_id}) => {
  await trackerApi.get(`/clients/delete/${_id}`);
  navigate('ClientList');
};
const deleteProduct = dispatch => async ({_id}) => {
  await trackerApi.get(`/products/delete/${_id}`);
  navigate('ProductList');
};
const deleteUser = dispatch => async ({_id}) => {
  await trackerApi.get(`/users/delete/${_id}`);
  navigate('UserList');
};

export const { Provider, Context } = createDataContext(
    searchReducer,
    { searchProduct, searchClient, searchUser, searchClientSumit, searchProductSumit, searchUserSumit, editClient, editProduct, editUser, createClient, createProduct, deleteClient, deleteProduct, deleteUser },
    []
);