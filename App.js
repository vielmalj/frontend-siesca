import React from 'react';
import {Image, StyleSheet, View, ScrollView} from 'react-native';
import { createAppContainer, createSwitchNavigator, SafeAreaView  } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';

import SignoutScreen from './src/screens/SignoutScreen';
import SigninScreen from './src/screens/SigninScreen';
import SignupScreen from './src/screens/SignupScreen';
import ProductDetailScreen from './src/screens/ProductDetailScreen';
import ProductListScreen from './src/screens/ProductListScreen';
import ProductEditScreen from './src/screens/ProductEditScreen';
import ProductCreateScreen from './src/screens/ProductCreateScreen';
import ClientDetailScreen from './src/screens/ClientDetailScreen';
import ClientListScreen from './src/screens/ClientListScreen';
import ClientEditScreen from './src/screens/ClientEditScreen';
import ClientCreateScreen from './src/screens/ClientCreateScreen';
import UserDetailScreen from './src/screens/UserDetailScreen';
import UserListScreen from './src/screens/UserListScreen';
import UserEditScreen from './src/screens/UserEditScreen';
import UserCreateScreen from './src/screens/UserCreateScreen';
import { Provider as AuthProvider } from './src/context/AuthContext';
import { Provider as SearchProvider } from './src/context/SearchContext';
import { setNavigator } from './src/navigationRef';
import ResolveAuthScreen from './src/screens/ResolveAuthScreen';

const drawerFlow = createDrawerNavigator({
  Productos: createStackNavigator({
    ProductList: ProductListScreen,
    ProductDetail: ProductDetailScreen,
    ProductEdit: ProductEditScreen,
    ProductCreate: ProductCreateScreen,
  }),
  Clientes: createStackNavigator({
    ClientList: ClientListScreen,
    ClientDetail: ClientDetailScreen,
    ClientEdit: ClientEditScreen,
    ClientCreate: ClientCreateScreen,
  }),
  Usuarios: createStackNavigator({
    UserList: UserListScreen,
    UserDetail: UserDetailScreen,
    UserEdit: UserEditScreen,
    UserCreate: UserCreateScreen,
  }),    
  Signout: SignoutScreen,
}, {
  contentComponent: (props) => (
    <SafeAreaView forceInset={{ top: 'always' }}>
      <View style={{height: 100,alignItems: 'center', justifyContent: 'center'}}>
          <Image source={require('./assets/iconos-01.png')} style={{height: 140, width:140, marginTop: 50}}></Image>
      </View>
      <ScrollView style={{margin: 10}}>
        <DrawerItems {...props} />
      </ScrollView>
    </SafeAreaView>
   )
});
const switchNavigator = createSwitchNavigator({
  ResolveAuth: ResolveAuthScreen,
  loginFlow: createStackNavigator({
    Signup: SignupScreen,
    Signin: SigninScreen,
  }),
  drawerFlow
});

const App = createAppContainer(switchNavigator);


export default () => {
  return (
    <AuthProvider>
      <SearchProvider>
         <App
           ref={(navigator) => {
             setNavigator(navigator);
           }}
         />
      </SearchProvider>
    </AuthProvider>
  );
};
