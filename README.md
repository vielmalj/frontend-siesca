        Fontend Siesca

Nos aseguramos de tener instalado Nodejs, si no es asi podemos
instalarlo desde la web

    https://nodejs.org/es/

Instalamos expo cli desde nuestro terminal

    npm install --global expo-cli

Desde el terminal en la carpeta del proyecto
instalamos las dependencias que utilizaremos:

    npm install react-navigation

    expo install react-native-gesture-handler react-native-reanimated
    react-native-screens react-native-safe-area-context @react-native-
    community/masked-view

    npm install react-navigation-stack @react-native-community/masked-view

    npm instal react-navigation-tabs

    npm install @expo/vector-icons

    npm install react-navigation-stack react-navigation-drawer

    npm install react-native-elements

    npm i axios

    expo install expo-linear-gradient

    expo install expo-image-picker

Para la conexion entre la pc y el telefono necesitamos tener
ngrok en nuestro proyecto para ello podemos hacerlo desde la pagina web

    https://ngrok.com/download

si lo instalamos desde la pagina web debemos descomprimirlo y pegar el archivo en
la carpeta del proyecto, otra forma de hacerlo es en el terminal

    npm i ngrok

luego de ello debemos abrir dos terminales, un para abrir el proyecto y otra para abrir ngrok

    npm start

    ngrok http 3000

Si es una cuenta gratuita ngrok nos brindara 8 horas de servicio, luego de esto debemos cerrarlo e iniciarlo nuevamente,
al abriri el ngrok este nos dara una direccion http, ejemplo

    http://3c43d5871aaf.ngrok.io

esta debemos colocarla en el archivo tracker.js de la carpeta api, en la linea 6 dentro de las comillas

En nuestro dispositivo movil debemos tener instalado la app de expo client, con ella escaneamos el codigo qr generado anteriormente y de esta, manera podremos probar nuestra app


